=================
SetInternetOnline
=================

Small console programs that disables ``Work Offline`` mode in Internet Explorer without requiring you to start IE and uncheck this option manually.


On my computer, Internet Explorer randomly goes offline and I can't (don't want) find why. I don't use IE, but Evernote use it's libraries and refuse to syncronize when this option is enabled. And it's too boring to start IE just to uncheck ``Work offline`` option (moreover, by magic it's enabled back again after a while).

Put this small program on your desktop or any other fast accessible location. After run, it's programmatically disbales ``Offline mode`` and your Evernote can syncronize again.

**Important**: Microsoft ``.NET Framework 2.0`` or above is required, your can download it from `Microsoft website <http://www.microsoft.com/net>`_ if it's not installed.
