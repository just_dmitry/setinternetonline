﻿using System;
using System.Runtime.InteropServices;
using System.Text;

namespace Parsec.SetInternetOnline {

    /// <summary>
    /// Thanks to http://www.rsdn.ru/forum/dotnet/1135428.flat for working sample
    /// </summary>
    public class Program {

        static void Main(string[] args) {
            INTERNET_CONNECTED_INFO ci = new INTERNET_CONNECTED_INFO();
            ci.dwConnectedState = INTERNET_STATE_CONNECTED;

            InternetSetOption(0,
                              INTERNET_OPTION_CONNECTED_STATE,
                              ref ci,
                              (uint)System.Runtime.InteropServices.Marshal.SizeOf(ci));
            Console.WriteLine("IE set to online mode");
        }

        //Some internet options
        const uint INTERNET_STATE_CONNECTED = 1;
        const uint INTERNET_OPTION_CONNECTED_STATE = 50;

        [StructLayout(LayoutKind.Sequential)]
        struct INTERNET_CONNECTED_INFO {
            public uint dwConnectedState;
            public uint dwFlags;
        }

        [DllImport("wininet.dll", EntryPoint = "InternetSetOptionA", CharSet = CharSet.Ansi)]
        private extern static bool InternetSetOption(int hInternet,
            uint dwOption,
            ref INTERNET_CONNECTED_INFO lpBuffer,
            uint dwBufferLength
            );
    }
}
